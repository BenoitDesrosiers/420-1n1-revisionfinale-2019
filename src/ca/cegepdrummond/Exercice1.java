package ca.cegepdrummond;

import java.util.ArrayList;
import java.util.Scanner;

public class Exercice1 {
    private ArrayList<Integer> liste = new ArrayList<>();


    public Exercice1(int[] valeurs) {
        for (int valeur: valeurs
        ) {
            liste.add(valeur);
        }
    }

    public void vide() {
        liste.clear();
    }

    public void ajoute() {
        int valeur;
        Scanner s = new Scanner(System.in);
        do {
            System.out.print("valeur > 0 (0 pour terminer)");
            valeur = s.nextInt();
            if(valeur > 0) {
                liste.add(valeur);
            } else if (valeur < 0) {
                System.out.println("plus grand que 0");
            }
        } while(valeur != 0);
    }


    public void affiche() {
        for (Integer valeur : liste
             ) {
            System.out.print(valeur +" ");
        }
    }

    public Integer valeurMax() {
        Integer max = liste.get(0);
        for (Integer valeur: liste
             ) {
            if(valeur > max) {
                max = valeur;
            }
        }
        return max;
    }

}
