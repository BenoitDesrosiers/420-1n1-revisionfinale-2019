package ca.cegepdrummond;

public class ExerciceRevision {

    public static void main(String[] args) {
        section1();
    }

    private static void section1() {
        /**********************
        Exercice 1a:

        Créez une classe s'appelant Exercice1

        Cette classe doit avoir une variable d'instance privée de
        type ArrayList qui servira à entreposer des objets entiers (Integer)

        Elle doit avoir un constructeur prenant un tableau (array)
            d'entiers (int) et qui initialise la variable privée de type ArrayList
            avec ce tableau.

        Créez ensuite la méthode affiche() qui affiche le contenu de l'ArrayList.
            Vous pouvez utiliser une boucle foreach pour afficher les éléments un après l'autre.


        Une fois ces étapes réalisées, vous pouvez enlever les commentaires
        sur les 3 prochaines lignes afin de tester votre code
         */
        /*
        Exercice1 x = new Exercice1(new int[] {3,4,6,7,3,2});
        x.affiche();
        System.out.println();
        */

        /**********************
        Exercice 1b:

        Ajoutez la méthode valeurMax() permettant de trouver la valeur maximum de l'ArrayList.
        ATTENTION: vous ne devez pas utiliser la méthode .max() des collections. Vous
        devez faire vous même le code pour trouver le max dans ArrayList.
        (Astuce: Référrez-vous au 2e exercices sur les Tableaux dans Stepik)

        Note: vous n'avez pas besoin de gérer le cas où la liste est vide.

        Une fois votre méthode créée, enlevez le commentaire sur la prochaine ligne
        afin de tester votre code. (devrait afficher 7 )
         */

        //System.out.println(x.valeurMax());

        /**********************
        Exercice 1c:

        Créez la  méthode vide() permettant de vider le contenu de l'ArrayList.

        Une fois votre méthode créée, enlevez le commentaire sur les 3 prochaines lignes
        afin de tester votre code.
         */

        /*
        x.vide();
        x.affiche();
        System.out.println();
         */


        /*
        Exercice 1d:

        Ajoutez la méthode ajoute() permettant d'ajouter des valeurs.
        L'entrée des données (scanner) est faite dans la méthode ajoute(). Elle ne prend
        donc aucun arguments.

        Ces valeurs doivent être positives.
        Si une valeur est négative, affichez un message d'erreur et
        continuez à demander des valeurs.
        L'usager doit entrer la valeur 0 pour terminer

        Une fois la méthode créée, enlevez le commentaire sur les 3 prochaines lignes.
         */
        /*
        x.ajoute();
        x.affiche();
        System.out.println();
        */

    }
}
